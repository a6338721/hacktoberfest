#include <bits/stdc++.h>

using namespace std;

#pragma region
// clang-format off
#define fastIO ios_base::sync_with_stdio(0); cin.tie(NULL); cout.tie(NULL);
#define int long long
#define newLine cout<<"\n";
#define fullLength(v) v.begin(), v.end()
#define rfullLength(v) v.rbegin(), v.rend()
#define fileIO freopen("input.txt", "r", stdin); freopen("output.txt", "w", stdout);
#define readOnly freopen("input.txt", "r", stdin);
typedef long long ll;

const ll maxx = 1e9 + 9;
const string yes = "Yes\n", no = "No\n";

#define readOnly freopen("input.txt", "r", stdin);

namespace ___debug {
    template <typename T> struct is_outputtable { template <typename C> static constexpr decltype(declval<ostream &>() << declval<const C &>(), bool()) test(int64_t) { return true; } template <typename C> static constexpr bool test(...) { return false; } static constexpr bool value = test<T>(int64_t()); };
    template <class T, typename V = decltype(declval<const T &>().begin()), typename S = typename enable_if<!is_outputtable<T>::value, bool>::type> void pr(const T &x);

    template <class T, typename V = decltype(declval<ostream &>() << declval<const T &>())> void pr(const T &x) { cout << x; }
    template <class T1, class T2> void pr(const pair<T1, T2> &x);
    template <class Arg, class... Args> void pr(const Arg &first, const Args &...rest) { pr(first); pr(rest...); }

    template <class T, bool pretty = true> void prContain(const T &x) { if (pretty) pr("{"); bool fst = 1; for (const auto &a : x) pr(!fst ? pretty ? ", " : " " : "", a), fst = 0; if (pretty) pr("}"); }

    template <class T> void pc(const T &x) { prContain<T, false>(x); pr("\n"); }
    template <class T1, class T2> void pr(const pair<T1, T2> &x) { pr("{", x.first, ", ", x.second, "}"); }
    template <class T, typename V, typename S> void pr(const T &x) { prContain(x); }
    void ps() { pr("\n"); }
    template <class Arg> void ps(const Arg &first) { pr(first); ps(); }
    template <class Arg, class... Args> void ps(const Arg &first, const Args &...rest) { pr(first, " "); ps(rest...); }
}
using namespace ___debug;

#define __pn(x) pr(#x, " = ")
#ifdef DEBUG_LOCALE
#define deb(...) pr("\033[1;31m"), __pn((__VA_ARGS__)), ps(__VA_ARGS__), pr("\033[0m"), cout << flush
#else
#define deb(...)
#endif

int lcm(int a, int b) {
  return (a * b) / __gcd(a, b);
}

bool isPrime(int num) {
  if (num < 2)
    return false;
  for (int x = 2; x * x <= num; x++) {
    if (num % x == 0)
      return false;
    }
  return true;
}

// clang-format on
#pragma endregion

// int findScore()

int solve() {
  int n, x, t;
  vector<int> sal, sb;
  vector<int>::iterator it;
  string s;

  cin >> n;
  // int matrix[2][n];
  vector<vector<int>> matrix(2, vector<int>(n));

  auto findScore = [&](int x, int y) {
    int sum = 0, sumup = 0, sumdown = 0;
    int presum = 0, sufsum = 0;
    for (int i = y; i < n - 1; i++) {
      sumup += matrix[0][i + 1];
      deb(sumup);
    }
    for (int i = 0; i < 2; i++) {
      for (int f = 1; f < y + 1; f++) presum += matrix[i][f];
    }
    sb.push_back(min(sumdown, sumup));
  };

  for (int i = 0; i < 2; i++)
    for (int f = 0; f < n; f++) cin >> matrix[i][f];

  for (int i = 0; i < 2; i++) {
    for (int f = 0; f < n; f++) {
      findScore(i, f);
    }
  }
  cout << *max_element(fullLength(sb));
  newLine
}

int32_t main() {
  // clang-format off
  fastIO
  #ifdef DEBUG_LOCALE
      readOnly
  #elif !defined (ONLINE_JUDGE)
      fileIO
  #endif

  int t = 1;

  cin >> t;
  while (t--)
  solve();

}
